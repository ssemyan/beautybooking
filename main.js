"use strict";

var express = require('express');
var config = require('./config');

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(config.database, {useMongoClient: true});

var controllers = require('./controllers')(mongoose);
var app = express();
var router = express.Router();

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var expressValidator = require('express-validator');
app.use(expressValidator());

controllers.init(app, router);
app.use('/api', router);

const port = 3000;
app.listen(port, function () {
	console.log('Example app listening on port 3000!');
});
