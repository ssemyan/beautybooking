var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

gulp.task('default', function () {
	nodemon({
		script: 'main.js',
		ext: 'js',
		ignore: ['./node_modules/**']
	})
});