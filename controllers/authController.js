module.exports = function (mongoose) {
	var authController = {};

	authController.init = function (app, router) {
		var jwt = require('jsonwebtoken');
		var config = require('../config');
		var moment = require('moment');

		var repository = require('../data/repository')(mongoose);
		var express = require('express');

		router.post('/login', function (req, res) {
			repository.getUser(req.body.email, function (err, user) {
				if (err) res.status(401).send(err);
				if (!user) {
					res.status(401).send('Invalid email or password.');
				} else {
					user.comparePassword(req.body.password, function (err, isMatch) {
						if (err) res.status(401).send(err);

						if (!isMatch) {
							res.status(401).send('Invalid email or password.')
						} else {
							var expires = moment().add(60, 'minutes');
							var token = jwt.sign(user, config.secret, {expiresIn: expires.valueOf()});

							res.json({token: token, expires: expires.format()});
						}
					});
				}
			});
		})
	}

	return authController;
}