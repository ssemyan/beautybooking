module.exports = function (mongoose) {
	var clientController = {};

	clientController.init = function (app, router) {

		var config = require('../config');
		var repository = require('../data/repository')(mongoose);
		var tokenChecker = require('./tokenChecker');
		var validationSchemas = require('../validation/validationSchemas');

		router.route('/clients')
			.post(function (req, res) {
				req.checkBody(validationSchemas.registrationSchema);
				req.getValidationResult().then(function (result) {
					var errors = result.array();
					if (errors.length > 0) {
						res.status(400).send(errors);
					} else {
						repository.addClient(
							req.body.name,
							req.body.email,
							req.body.password,
							function (err, newClient) {
								if (err) res.status(500).send(err);
								res.status(201).json(newClient);
							})
					}
				});
			})
			.get(function (req, res) {
				repository.findAllClients(function (err, clients) {
					if (err) res.status(500).send(err);
					res.json(clients)
				})
			});
	}

	return clientController;
}