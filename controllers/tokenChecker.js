module.exports = {

	authenticateToken: function (req, res, next) {
		var jwt = require('jsonwebtoken');
		var config = require('../config');

		var token = (req.body && req.body.access_token) ||
			(req.query && req.query.access_token) ||
			req.headers['x-access-token'];

		if (token) {
			jwt.verify(token, config.secret, function (err, decoded) {
				if (err) {
					return res.json({success: false, message: 'Failed to authenticate token.'});
				} else {
					req.decoded = decoded;
					console.log(decoded);
					next();
				}
			});
		} else {
			return res.status(401).send('The token is required.');
			next();
		}
	}
}