module.exports = function (mongoose) {
	var timeSlotController = {};

	timeSlotController.init = function (app, router) {

		var repository = require('../data/repository')(mongoose);
		var tokenChecker = require('./tokenChecker');

		router.patch('/timeslots/:timeslotId', tokenChecker.authenticateToken, function (req, res) {
			repository.updateTimeSlotAvailability(
				req.params.timeslotId,
				req.body.clientId,
				function (err, updated) {
					if (err) res.status(500).send(err);
					res.json(updated.timeSlots[0]);
				});
		});

		router.get('/timeslots/:professionalId', function (req, res) {
			repository.getProfessionalTimeSlots(
				req.params.professionalId,
				function (err, timeSlots) {
					if (err) res.status(500).send(err);
					res.json(timeSlots);
				});
		});

		router.post('/timeslots', tokenChecker.authenticateToken, function (req, res) {
			var newDoc = repository.addTimeSlotsToProfessional(
				req.body.professionalId,
				req.body.timeSlots,
				function (err, updated) {
					if (err) res.status(500).send(err);
					res.json(updated);
				});
		});
	}

	return timeSlotController;
}