module.exports = function (mongoose) {
	var controllers = {};

	var professionalController = require("./professionalController")(mongoose);
	var timeSlotController = require("./timeSlotController")(mongoose);
	var clientController = require("./clientController")(mongoose);
	var authController = require("./authController")(mongoose);

	controllers.init = function (app, router) {

		professionalController.init(app, router);
		timeSlotController.init(app, router);
		clientController.init(app, router);
		authController.init(app, router);
	}

	return controllers;
}
