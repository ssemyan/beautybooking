module.exports = function (mongoose) {
	var professionalController = {};

	professionalController.init = function (app, router) {

		var repository = require('../data/repository')(mongoose);

		router.get('/professionals', function (req, res) {
			repository.findAllProfessionals(function (err, professionals) {
				if (err) res.status(500).send(err);
				res.json(professionals)
			});
		});
		router.get('/professionals/:professionalId', function (req, res) {
			repository.findProfessionalById(req.params.professionalId,
				function (err, professionals) {
					if (err) res.status(500).send(err);
					res.json(professionals)
				});
		});
		router.post('/professionals', function (req, res) {
			req.checkBody(validationSchemas.registrationSchema);
			req.getValidationResult().then(function (result) {

				var errors = result.array();
				if (errors.length > 0) {
					res.status(400).send(errors);
				} else {

					var requestBody = req.body;
					repository.addProfessional(
						requestBody.name,
						requestBody.email,
						requestBody.password,
						function (err, professional) {
							if (err) res.status(500).send(err);
							res.status(201).json(professional);
						});
				}
			});
		});
	}

	return professionalController;
}
