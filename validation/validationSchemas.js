module.exports = {
	registrationSchema: {
		"email": {
			notEmpty: true,
			matches: {
				options: [".+@.+\..+", "g"],
				errorMessage: "Invalid email."
			}
		},
		"password": {
			notEmpty: true,
			isLength: {
				options: [{min: 6}],
				errorMessage: "The password must be at least 6 characters."
			},
			matches: {
				options: ["(?=.*[a-zA-Z]+)(?=.*[0-9]+)(?=.*[!*@#$%^&()-+]+).*", "g"],
				errorMessage: "The password must be alphanumeric."
			},
			errorMessage: "Invalid password."
		}
	}
}