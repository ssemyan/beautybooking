module.exports = function (mongoose) {
	var repository = {};

	var Professional = require('./schemas/professional')(mongoose);
	var Client = require('./schemas/client')(mongoose);
	var User = require('./schemas/user')(mongoose);

	repository.findAllProfessionals = function (cb) {
		Professional.find({}, cb);
	};

	repository.findProfessionalById = function (professionalId, cb) {
		Professional.findOne({_id: professionalId}, cb);
	};

	repository.addProfessional = function (name, email, password, cb) {
		var newUser = new User({email: email, password: password});
		newUser.save();

		var newEntry = new Professional({name: name, user: newUser});
		newEntry.save(cb);
	};

	repository.getProfessionalTimeSlots = function (professionalId, cb) {
		Professional.find({_id: professionalId}, '-_id timeSlots', cb)
	};

	repository.addTimeSlotsToProfessional = function (professionalId, timeSlots, cb) {
		Professional.findOneAndUpdate(
			{_id: professionalId},
			{$pushAll: {timeSlots: timeSlots}},
			{new: true, fields: '_id name timeSlots'},
			cb
		)
	};

	repository.updateTimeSlotAvailability = function (timeSlotId, clientId, cb) {
		Professional.findOneAndUpdate(
			{"timeSlots._id": timeSlotId},
			{$set: {"timeSlots.$.clientId": clientId}},
			{new: true, fields: {_id: 0, timeSlots: {$elemMatch: {_id: timeSlotId}}}},
			cb);
	};

	repository.addClient = function (name, email, password, cb) {
		var newUser = new User({email: email, password: password});
		newUser.save();

		var newClient = new Client({name: name, user: newUser});
		newClient.save(cb);
	};

	repository.findAllClients = function (cb) {
		Client.find({}, cb);
	};

	repository.getUser = function (email, cb) {
		User.findOne({email: email}, cb);
	}

	return repository;
}