module.exports = function (mongoose) {
	var bcrypt = require('bcrypt');
	var config = require('../../config');

	var Schema = mongoose.Schema;

	var userSchema = new Schema({
		email: {type: String, required: true, index: {unique: true}},
		password: {type: String, required: true}
	});

	userSchema.set('toJSON', {
		transform: function (doc, ret, options) {
			delete ret.__v;
			delete ret.password;
			return ret;
		}
	});

	userSchema.pre('save', function (next) {
		var user = this;

		if (!user.isModified('password')) return next();

		bcrypt.hash(user.password, config.saltWorkFactor, function (err, hash) {
			if (err) return next(err);
			user.password = hash;
			next();
		});
	});

	userSchema.methods.comparePassword = function (candidatePassword, cb) {
		bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
			if (err) return cb(err);
			cb(null, isMatch);
		});
	};

	if (mongoose.models.User)
		user = mongoose.model('User');
	else
		user = mongoose.model('User', userSchema);

	return user;
}