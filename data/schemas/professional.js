module.exports = function (mongoose) {
	var Schema = mongoose.Schema;

	var professionalSchema = new Schema({
		name: {type: String, required: true},
		timeSlots: [
			{
				startTime: {type: Date, required: true},
				endTime: {type: Date, required: true},
				clientId: Schema.Types.ObjectId
			}],
		user: {type: String, required: true, ref: 'User'}
	});

	professionalSchema.set('toJSON', {
		transform: function (doc, ret, options) {
			var retJson = {
				id: ret._id,
				name: ret.name,
				timeSlots: ret.timeSlots
			};
			return retJson;
		}
	});

	var professional;

	if (mongoose.models.Professional)
		professional = mongoose.model('Professional');
	else
		professional = mongoose.model('Professional', professionalSchema);

	return professional;
}
