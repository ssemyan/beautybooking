module.exports = function (mongoose) {
	var Schema = mongoose.Schema;

	var clientSchema = new Schema({
		name: {type: String, required: true},
		user: {type: String, required: true, ref: 'User'}
	});

	clientSchema.set('toJSON', {
		transform: function (doc, ret, options) {
			var retJson = {
				id: ret._id,
				name: ret.name
			};
			return retJson;
		}
	});

	var client;

	if (mongoose.models.Client)
		client = mongoose.model('Client');
	else
		client = mongoose.model('Client', clientSchema);

	return client;
}